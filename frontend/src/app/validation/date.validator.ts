import { FormControl } from '@angular/forms';
import * as moment from 'moment';

export function DateValidator(format = 'MM/DD/YYYY'): any {
  return (control: FormControl): { [key: string]: any } => {
    const val = moment(control.value, format, true);

    console.log('Validating date');
    console.log(val);

    // Date should conform to the format 'MM/dd/YYYY'.
    if (!val.isValid()) {
      return { invalidDate: true };
    }

    // Date should not be in the future.
    if (val.isValid() && moment().isSameOrBefore(val)) {
      return { dateInFuture: true };
    }
    return null;
  };
}
