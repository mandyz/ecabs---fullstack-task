import { Injectable } from '@angular/core';
import { Book } from '../model/book';
import {Observable, of} from 'rxjs';
import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private basePath = 'http://localhost:1234';

  private book1: Book = {
    title: "Book1 Title",
    author: "Book1 Author",
    published: "2012-12-06",
    notes: "Book1 Note."
  }

  private book2: Book = {
    title: "Book2 Title",
    author: "Book2 Author",
    published: "2012-12-06",
    notes: "Book2 Note."
  }

  constructor() { }

  getBooks(): Observable<Book[]> {
    return of([this.book1, this.book2]);
  }

  addBook(book: Book): Observable<Book> {
    book.id = 500;
    return of(book);
  }

  getBookById(bookId: number): Observable<Book> {
    // Setting book id of book1 to the requested id and returning the book.
    this.book1.id = bookId;
    return of(this.book1);
  }
}
