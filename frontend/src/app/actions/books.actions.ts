import { Injectable} from '@angular/core';
import { Action } from '@ngrx/store';
import { Book } from '../model/book';

export const ADD_BOOK = '[Book] Add';
export const LOAD_ALL_BOOKS = '[Book] LoadAll';
export const LOAD_ALL_BOOKS_SUCCESS = '[Book] LoadAll Success';
export const LOAD_ALL_BOOKS_FAILURE = '[Book] LoadAll Failure';

export class AddBook implements Action {
  readonly type = ADD_BOOK;

  constructor(public payload: Book) {}
}

export class LoadAllBooks implements Action {
  readonly type = LOAD_ALL_BOOKS;

  // TODO does this need a constructor?
}

export class LoadAllBooksSuccess implements Action {
  readonly type = LOAD_ALL_BOOKS_SUCCESS;

  constructor(public payload: Book[]) {}
}

export class LoadAllBooksFailure implements Action {
  readonly type = LOAD_ALL_BOOKS_FAILURE;

  constructor(public payload: any) {}
}

export type Actions = AddBook | LoadAllBooks | LoadAllBooksSuccess | LoadAllBooksFailure;
