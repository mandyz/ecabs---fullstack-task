import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import * as BookActions from '../actions/books.actions';
import { BookService } from '../book.service';

@Injectable()
export class BookEffects {

  constructor(private actions$: Actions, private bookService: BookService) {}

  @Effect()
  loadBooks$ = this.actions$.ofType(BookActions.LOAD_ALL_BOOKS)
    .pipe(
      switchMap( () => {
        return this.bookService.getBooks().pipe(
          map(books => new BookActions.LoadAllBooksSuccess(books)),
          catchError(error => of(new BookActions.LoadAllBooksFailure(error)))
        );
        })
    );
}
