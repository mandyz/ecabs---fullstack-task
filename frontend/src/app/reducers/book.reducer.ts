import { Book } from '../model/book';
import * as BookActions from './../actions/books.actions';
import { BookService } from '../book.service';

export function bookReducer(state: Book[] = [], action: BookActions.Actions) {
  switch (action.type) {

    case BookActions.ADD_BOOK:
      return [action.payload, ...state];

    case BookActions.LOAD_ALL_BOOKS: // This action is listened to by an effect.
      return state;

    case BookActions.LOAD_ALL_BOOKS_SUCCESS:
      console.log(action.payload);
      const data = action.payload;
      return data;

    case BookActions.LOAD_ALL_BOOKS_FAILURE:
      console.log(action.payload);
      return state;

    default:
      return state;
  }
}

