import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BookformComponent } from "./bookform/bookform.component";
import { BooksComponent } from "./books/books.component";
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import {bookReducer} from "./reducers/book.reducer";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        BookformComponent,
        BooksComponent
      ],
      imports: [
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatNativeDateModule,
        HttpClientModule,
        StoreModule.forRoot({
          book: bookReducer
        }),
        NoopAnimationsModule
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));

  it('should include app-bookfrom tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-bookform')).toBeTruthy();
  }));

  it('should include app-books tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-books')).toBeTruthy();
  }));
});
