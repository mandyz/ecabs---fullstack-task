import { Component, OnInit } from '@angular/core';
import { BookService} from '../book.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Book } from '../model/book';
import { AppState } from '../app.state';
import * as BookActions from '../actions/books.actions';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books: Observable<Book[]>;

  constructor(private store: Store<AppState>, private bookService: BookService) {}


  ngOnInit() {
    this.books = this.store.select('book');
    // Triggering a '[Book] LoadAll' action on the store.
    // This will be redirected to an Effect which will retrieve all Books from the server and trigger a '[Book] LoadAll Success' or '[Book] LoadAll Failure' action.
    this.store.dispatch(new BookActions.LoadAllBooks());
    // this.bookService.getBooks().subscribe(books => this.books = books); // Note: this was moved to an effect.
  }
}
