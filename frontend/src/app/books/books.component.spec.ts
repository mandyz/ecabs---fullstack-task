import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooksComponent } from './books.component';
import {MatExpansionModule} from "@angular/material";
import {Store, StoreModule} from "@ngrx/store";
import {bookReducer} from "../reducers/book.reducer";
import {HttpClientModule} from "@angular/common/http";

describe('BooksComponent', () => {
  let component: BooksComponent;
  let fixture: ComponentFixture<BooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksComponent ],
      imports: [
        MatExpansionModule,
        StoreModule.forRoot({
          book: bookReducer
        }),
        HttpClientModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create books (list) component', () => {
    expect(component).toBeTruthy();
  });
});
