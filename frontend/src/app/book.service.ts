import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Book} from './model/book';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private basePath = 'http://localhost:1234';

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.basePath + '/books');
  }

  addBook(book: Book): Observable<Book> {
    return this.http.post<Book>(this.basePath + '/books', book);
  }

  getBookById(bookId: number): Observable<Book> {
    return this.http.get<Book>(this.basePath + '/books/' + bookId);
  }
}
