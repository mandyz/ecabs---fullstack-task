import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookformComponent } from './bookform.component';
import { MatButtonModule, MatInputModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import {HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {Store, StoreModule} from "@ngrx/store";
import {bookReducer} from "../reducers/book.reducer";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import * as moment from 'moment';

describe('BookformComponent', () => {
  let component: BookformComponent;
  let fixture: ComponentFixture<BookformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookformComponent ],
      imports: [
        MatButtonModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        HttpClientModule,
        ReactiveFormsModule,
        StoreModule.forRoot({
          book: bookReducer
        }),
        NoopAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a book form', () => {
    expect(component).toBeTruthy();
  });


  it('should validate title', () => {
    component.title.setValue('');
    expect(component.title.valid).toBeFalsy();

    component.title.setValue('Title');
    expect(component.title.valid).toBeTruthy();
  })

  it('should validate author', () => {
    component.authorName.setValue('');
    expect(component.authorName.valid).toBeFalsy();

    component.authorName.setValue('Author');
    expect(component.authorName.valid).toBeTruthy();
  })

  it('should validate published date', () => {
    component.published.setValue('');
    expect(component.authorName.valid).toBeFalsy();

    // TODO this is not trigerring validation
    // console.log('Setting date to ' + moment().format('MM/DD/YYYY').toString());
    // component.authorName.setValue(moment().format('MM/DD/YYYY').toString());
    // console.log(component.published.valid);
    // expect(component.published.valid).toBeTruthy();
    //
    // component.authorName.setValue(moment().add(1, 'd').format('MM/DD/YYYY').toString());
    // expect(component.published.valid).toBeFalsy();
  })
});
