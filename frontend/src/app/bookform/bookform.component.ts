import { Component, OnInit } from '@angular/core';
import { Book } from '../model/book';
import { BookService  } from '../book.service';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import * as BookActions from './../actions/books.actions';
import { DateValidator } from '../validation/date.validator';
import * as moment from 'moment';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  title = new FormControl('', [Validators.required, Validators.maxLength(255)]);
  authorName = new FormControl('', [Validators.required, Validators.maxLength(255)]);
  published = new FormControl('', [Validators.required, DateValidator()]);
  notes = new FormControl('');

  constructor(private bookService: BookService, private store: Store<AppState>) {}

  ngOnInit() {
  }

  onSubmit() {
    // TODO Consider adding check on global form when changing from FormControl to GroupForm.
    const book: Book = {
      title: this.title.value,
      author: this.authorName.value,
      published: moment(this.published.value).format('YYYY-MM-DD'),
      notes: this.notes.value
    };

    // Saving book to library.
    this.bookService.addBook(book).subscribe(createdBook => {
      // Refresh the model.
      this.refreshModel();

      // Dispatching Book added event to the store.
      this.store.dispatch(new BookActions.AddBook(createdBook));
    });
  }

  private refreshModel() {
    // TODO when changing from FormControl to GroupForm this can be reduced to just one reset on the whole GroupForm.
    this.title.reset();
    this.authorName.reset();
    this.published.reset();
    this.notes.reset();
  }

  getTitleErrorMessage() {
    return this.title.hasError('required') ? 'A title is required.' :
      this.title.hasError('maxlength') ? 'Title may contain a maximum of 255 characters.' : '';
  }

  getAuthorErrorMessage() {
    return this.authorName.hasError('required') ? 'An author is required.' :
      this.authorName.hasError('maxlength') ? 'Author may contain a maximum of 255 characters.' : '';
  }

  getPublishedErrorMessage() {
    return this.published.hasError('required') ? 'A published date is required.' :
      this.published.hasError('invalidDate') ? 'Published date must conform to MM/dd/YYYY.' :
        this.published.hasError('dateInFuture') ? 'Published date cannot be in the future.' : '';
  }

}
