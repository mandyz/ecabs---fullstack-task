package mnd.controller;

import io.swagger.api.BooksApi;
import mnd.model.Book;
import mnd.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * BookController
 *
 * @author Mandy Zammit
 */
@RestController
@CrossOrigin
public class BookController implements BooksApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService){
        this.bookService = bookService;
    }

    @Override
    public ResponseEntity<Book> getBookById(Integer bookId){

        Optional<Book> book = bookService.getBook(bookId);
        return book.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<List<Book>> getBooks() {
        LOGGER.debug("Retrieving all books.");
        return ResponseEntity.ok(bookService.getAllBooks());
    }

    @Override
    public ResponseEntity<Book> addBook(Book book){
        LOGGER.debug("Attempting to create Book {}", book);

        Book newBook = bookService.addBook(book);
        LOGGER.debug("Successfully created Book {}", book);
        return ResponseEntity.ok(newBook);
    }
}
