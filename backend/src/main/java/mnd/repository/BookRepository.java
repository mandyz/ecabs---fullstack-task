package mnd.repository;

import mnd.model.Book;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * BookRepository
 *
 * @author Mandy Zammit
 */
@Component
public class BookRepository {

    private static AtomicInteger lastId = new AtomicInteger(0);
    private Map<Integer, Book> books = new HashMap<>();

    public BookRepository(){
        // Initialising some dummy books.
        int id1 = lastId.incrementAndGet();
        books.put(id1, new Book().author("Jack Black").title("A book has no title").id(id1).published(LocalDate.of(2010, 12, 3)).notes("A book is no one."));

        int id2 = lastId.incrementAndGet();
        books.put(id2, new Book().author("John Doe").title("The book of John").id(id2).published(LocalDate.of(2012, 6, 12)).notes("This is a note about 'The Book of John'."));
    }
    /**
     * Adds a book to the Book Repository.
     * @param book
     */
    public Book addBook(Book book){
        int id = lastId.incrementAndGet();
        Book newBook = book.id(id);
        books.put(id, newBook);

        return newBook;
    }

    /**
     * Retrieves a book (if any) by the specified ID from the Book Repository.
     * @param id
     * @return
     */
    public Book getBookById(int id){
        return books.get(id);
    }

    /**
     * Retrieves all books from the Book Registry.
     * @return
     */
    public List<Book> getAllBooks(){
        return new ArrayList<>(books.values());
    }
}
