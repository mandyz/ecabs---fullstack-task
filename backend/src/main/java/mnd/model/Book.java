package mnd.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * mnd.model.Book
 *
 * @author Mandy Zammit
 */
public class Book {

    private int id;

    @NotNull
    @Size(max=255)
    private String author;

    @NotNull
    @Size(max=255)
    private String title;

    @NotNull
    @PastOrPresent
    private LocalDate published;

    private String notes;

    public Book(){}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getPublished() {
        return published;
    }

    public void setPublished(LocalDate published) {
        this.published = published;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    // TODO consider using Builder Pattern.
    public Book id(int id) {
        this.id = id;
        return this;
    }

    public Book author(String author) {
        this.author = author;
        return this;
    }

    public Book title(String title) {
        this.title = title;
        return this;
    }

    public Book published(LocalDate published) {
        this.published = published;
        return this;
    }

    public Book notes(String notes) {
        this.notes = notes;
        return this;
    }
}
