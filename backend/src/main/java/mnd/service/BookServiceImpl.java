package mnd.service;

import mnd.model.Book;
import mnd.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * BookServiceImpl
 *
 * @author Mandy Zammit
 */
@Component
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @Override
    public Book addBook(Book book) {
        return bookRepository.addBook(book);
    }

    @Override
    public Optional<Book> getBook(int bookId) {
       return Optional.ofNullable(bookRepository.getBookById(bookId));
    }

    @Override
    public List<Book> getAllBooks() {
       return bookRepository.getAllBooks();
    }
}
