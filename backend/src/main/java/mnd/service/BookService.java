package mnd.service;

import mnd.model.Book;

import java.util.List;
import java.util.Optional;

/**
 * BookService
 *
 * @author Mandy Zammit
 */
public interface BookService {

    /**
     * Adds the specified book to the Book Repository.
     * @param book
     */
    Book addBook(Book book);

    /**
     * Retreives a book by ID from the Book Repository.
     * @param bookId - The book id.
     * @return
     */
    Optional<Book> getBook(int bookId);

    /**
     * Reteive all books from the Book Repository.
     * @return - list of books.
     */
    List<Book> getAllBooks();


}
