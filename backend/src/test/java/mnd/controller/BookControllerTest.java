package mnd.controller;

import mnd.model.Book;
import mnd.service.BookService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * BookControllerTest
 *
 * @author Mandy Zammit
 */
@RunWith(MockitoJUnitRunner.class)
public class BookControllerTest {

    @Mock
    BookService bookService;
    @InjectMocks
    BookController bookController;

    @Test
    public void testGetBookById(){
        int bookId = 1;
        when(bookService.getBook(bookId))
                .thenReturn(Optional.of(new Book().id(bookId)));

        bookController.getBookById(bookId);

        verify(bookService).getBook(bookId);
    }

    @Test
    public void testGetBooks(){
        bookController.getBooks();

        verify(bookService).getAllBooks();
    }

    @Test
    public void testAddBook(){
        Book book = new Book();
        bookController.addBook(book);

        verify(bookService).addBook(book);
    }
}
