package mnd.service;

import mnd.model.Book;
import mnd.repository.BookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * BookServiceTest
 *
 * @author Mandy Zammit
 */
@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    @Mock
    private BookRepository bookRepository;
    @InjectMocks
    private BookServiceImpl bookService;


    @Test
    public void testAddBook(){
        Book book = new Book();

        when(bookRepository.addBook(book))
                .thenReturn(book);

        bookService.addBook(book);
        verify(bookRepository).addBook(book);
    }

    @Test
    public void testGetAllBooks(){
        bookService.getAllBooks();
        verify(bookRepository).getAllBooks();
    }

    @Test
    public void testGetBookById(){
        int bookId = 1;
        bookService.getBook(bookId);
        verify(bookRepository).getBookById(bookId);
    }
}
