package mnd.respository;

import mnd.model.Book;
import mnd.repository.BookRepository;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;

/**
 * BookRepositoryTest
 *
 * @author Mandy Zammit
 */
public class BookRepositoryTest {

    private BookRepository bookRepository;

    @Before
    public void setup(){
         bookRepository= new BookRepository();
    }

    @Test
    public void testAddBook(){
        Book book = new Book()
                .title("The Book")
                .author("The Author")
                .published(LocalDate.now())
                .notes("");

        Book newlyCreatedBook = bookRepository.addBook(book);

        // Verify that the same book content is returned.
        assertThat(newlyCreatedBook)
                .isEqualTo(book.id(newlyCreatedBook.getId())); //Note: Id is not set as part of the original book (i.e. before saving).
    }

    @Test
    public void testGetBook(){
        Book book = new Book()
                .title("The Book")
                .author("The Author")
                .published(LocalDate.now())
                .notes("");

        Book newlyCreatedBook = bookRepository.addBook(book);
        int newlyCreatedBookId = newlyCreatedBook.getId();

        assertThat(bookRepository.getBookById(newlyCreatedBookId))
                .isEqualTo(book.id(newlyCreatedBookId));
    }

    @Test
    public void testGetAllBooks(){
        Book book = new Book()
                .title("The Book")
                .author("The Author")
                .published(LocalDate.now())
                .notes("");

        Book newlyCreatedBook = bookRepository.addBook(book);

        assertThat(bookRepository.getAllBooks())
                .contains(newlyCreatedBook);
    }
}
